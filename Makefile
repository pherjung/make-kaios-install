PORT_DEVICE = localfilesystem:/data/local/debugger-socket
PORT_LOCAL = 6000
XPCSHELL = ${pwd}/bin/xulrunner-sdk/bin/xpcshell
ID ?= ${shell basename ${FOLDER} | tr A-Z a-z}

package:
	@echo "ZIPPING ${FOLDER} into application.zip"
	@cd ${FOLDER} && zip -Xr ./application.zip ./* -x application.zip *.appcache

packaged:
	@echo "PUSHING *${ID}* as packaged app"
	@adb push ${FOLDER}/application.zip /data/local/tmp/b2g/${ID}/application.zip

hosted:
	@echo "PUSHING *${ID}* as hosted app"
	@adb push ${FOLDER}/manifest.webapp /data/local/tmp/b2g/${ID}/manifest.webapp
	@adb push ${FOLDER}/metadata.json /data/local/tmp/b2g/${ID}/metadata.json

install:
	@echo "FORWARDING device port $(PORT_DEVICE) to $(PORT_LOCAL)"
	@adb forward tcp:$(PORT_LOCAL) $(PORT_DEVICE)
	env LD_LIBRARY_PATH=${pwd}/bin/xulrunner-sdk/bin ${XPCSHELL} install.js ${ID} $(PORT_LOCAL)
